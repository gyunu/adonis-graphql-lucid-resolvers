const { parseRequest } = require('./helpers')
const index = (model) => async (root, args, context) => {
  const query = model.query()
  const result = await parseRequest({ query, args })

  if (typeof model._localize === 'function') {
    let localizedResult = []
    for (const res of result.rows) {
      const local = await model._localize(res)
      localizedResult.push(res)
    }

    return { data: localizedResult, pagination: result.pages }
  }

  return { data: result.toJSON().data, pagination: result.pages }
}

module.exports = index
