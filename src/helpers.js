const getOperator = (op) => {
  ops = {
    eq: '=',
    neq: '!=',
    lt: '<',
    gt: '>',
    gte: '>=',
    lte: '<=',
    like: 'like'
  }

  return ops[op]
}

const order = ({ query, args }) => {
  if (args.order) {
    if (args.order.orderBy) {
      query.orderBy(args.order.orderBy.field, args.order.orderBy.dir || 'asc')
    }
  }

  return {
    query,
    args
  }
}

const filter = ({ query, args }) => {
  if (args.filter) {
    if (args.filter.where) {
      query.where(args.filter.where.field, getOperator(args.filter.where.comparison), args.filter.where.value)
    }

    if (args.filter.whereNot) {
      query
        .whereNot(args.filter.whereNot.field, getOperator(args.filter.whereNot.comparison), args.filter.whereNot.value)
        .fetch()
    }

    if (args.filter.whereIn) {
      query.whereIn(args.filter.whereIn.field, args.filter.whereIn.values).fetch()
    }

    if (args.filter.whereNull) {
      query.whereNull(args.filter.whereNull.field).fetch()
    }

    if (args.filter.whereNotNull) {
      query.whereNotNull(args.filter.whereNotNull.field).fetch()
    }

    if (args.filter.whereNotBetween) {
      query.whereNotBetween(args.filter.whereNotBetween.field, [
        args.filter.whereNotBetween.start,
        args.filter.whereNotBetween.end
      ])
    }
  }

  return { query, args }
}

const paginate = async ({ query, args }) => {
  if (args.pagination) {
    return query.paginate(args.pagination.page, args.pagination.perPage)
  }

  return query.paginate(1)
}

module.exports = {
  parseRequest: async ({ query, args }) => paginate(order(filter({ query, args })))
}
