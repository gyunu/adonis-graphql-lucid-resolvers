const create = (model) => async (root, data) => {
  const result = await model.create(data)
  return result
}

module.exports = create
