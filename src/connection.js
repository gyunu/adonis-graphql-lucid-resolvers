const connection = (model, field) => async (data) => {
  if (typeof data[field] !== 'function') {
    const m = await model.find(data.id)
    const e = await m[field]().fetch()
    return e.toJSON()
  }
  else {
    const e = await data[field]().fetch()
    return e.toJSON()
  }
}

module.exports = connection
