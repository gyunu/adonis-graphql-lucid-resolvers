const read = (model) => async (root, { id }) => {
  const result = await model.find(id)
  return result
}

module.exports = read
