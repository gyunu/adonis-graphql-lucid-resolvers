const remove = (model) => async (root, { id }) => {
  const result = await model.find(id)
  await result.delete(id)
  return result
}

module.exports = remove
