const update = (model) => async (root, { id, ...data }) => {
  const result = await model.find(id)
  result.merge(data)
  result.save()
  return result
}

module.exports = update
