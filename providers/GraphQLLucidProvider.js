const { ServiceProvider } = require.main.require('@adonisjs/fold')

class GraphQLLucidProvider extends ServiceProvider {
  register () {
    this.app.singleton('Adonis/Addons/GraphQLLucid', () => {
      const connection = require('../src/connection')
      const create = require('../src/create')
      const index = require('../src/index')
      const read = require('../src/read')
      const remove = require('../src/remove')
      const update = require('../src/update')

      return { connection, create, index, read, remove, update }
    })
  }
}

module.exports = GraphQLLucidProvider
